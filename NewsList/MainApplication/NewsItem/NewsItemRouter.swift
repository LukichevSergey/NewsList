//
//  NewsItemRouter.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import Foundation

// MARK: Protocol - NewsItemPresenterToRouterProtocol (Presenter -> Router)
protocol NewsItemPresenterToRouterProtocol: AnyObject {
    func navigateToWebView(withLink link: String)
}

final class NewsItemRouter {

    // MARK: Properties
    weak var view: NewsItemRouterToViewProtocol!
}

// MARK: Extension - NewsItemPresenterToRouterProtocol
extension NewsItemRouter: NewsItemPresenterToRouterProtocol {
    func navigateToWebView(withLink link: String) {
        let webView = WKWebViewController(with: link)
        view.presentView(view: webView)
    }
}
