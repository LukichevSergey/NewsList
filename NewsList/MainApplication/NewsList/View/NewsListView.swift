//
//  NewsListView.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//

import UIKit

protocol NewsListViewDelegate: AnyObject {
    func articleTapped(with indexPath: IndexPath)
    func tableOffsetInBottomPosition()
    func refreshData()
}

protocol NewsListViewInterface: AnyObject {
    func setArticlesData(_ data: [Article])
}

final class NewsListView: UIView {
    
    // MARK: - Section
    enum Section {
        case main
    }
    
    // MARK: - Scroll direction
    enum ScrollingDirection {
        case up, down
    }
    
    private var lastScrollOffsetYPosition: CGFloat = 0
    
    private lazy var refreshControl = UIRefreshControl()
    
    weak var delegate: NewsListViewDelegate?
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: NewsTableViewCell.reuseIdentifier)
        tableView.keyboardDismissMode = .onDrag
        tableView.backgroundColor = .white
        tableView.separatorColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        return tableView
    }()
    
    private lazy var dataSource = UITableViewDiffableDataSource<Section, Article>(tableView: tableView) { [weak self] tableView, indexPath, item in
        
        guard let self,
              let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.reuseIdentifier, for: indexPath) as? NewsTableViewCell
        else {
            return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        cell.selectionStyle = .none
        cell.configure(with: item)

        return cell
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureUI()
    }
    
    @objc private func refreshData() {
        delegate?.refreshData()
    }
}

extension NewsListView {
    private func configureUI() {
        backgroundColor = .white
        
        addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
        ])
    }
    
    private func tableOffsetInBottomPosition(multiply: CGFloat = 1) -> Bool {
        let height = tableView.frame.size.height * multiply
        let contentYoffset = tableView.contentOffset.y - tableView.contentInset.bottom
        let distanceFromBottom = tableView.contentSize.height - contentYoffset
        return distanceFromBottom <= height
    }
}

extension NewsListView: NewsListViewInterface {
    func setArticlesData(_ data: [Article]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Article>()
        
        snapshot.appendSections([.main])
        snapshot.appendItems(data, toSection: .main)
        
        dataSource.apply(snapshot, animatingDifferences: false) {
            self.refreshControl.endRefreshing()
        }
    }
}

extension NewsListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height / 3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.articleTapped(with: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDirection: ScrollingDirection = lastScrollOffsetYPosition > scrollView.contentOffset.y ? .down : .up
        
        lastScrollOffsetYPosition = scrollView.contentOffset.y
        guard scrollDirection == .up else { return }
        
        if tableOffsetInBottomPosition(multiply: 2), scrollView.contentOffset.y > 0 {
            delegate?.tableOffsetInBottomPosition()
        }
    }
}
