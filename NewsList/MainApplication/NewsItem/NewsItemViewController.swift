//
//  NewsItemViewController.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import UIKit

// MARK: Protocol - NewsItemPresenterToViewProtocol (Presenter -> View)
protocol NewsItemPresenterToViewProtocol: AnyObject {
    func setArticleView(from article: Article)
}

// MARK: Protocol - NewsItemRouterToViewProtocol (Router -> View)
protocol NewsItemRouterToViewProtocol: AnyObject {
    func presentView(view: UIViewController)
    func pushView(view: UIViewController)
}

final class NewsItemViewController: UIViewController {
    
    // MARK: - Property
    var presenter: NewsItemViewToPresenterProtocol!
    private lazy var contentView = NewsItemView()

    // MARK: - init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func loadView() {
        view = contentView
        contentView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.viewDidLoad()
    }
}

// MARK: Extension - NewsItemPresenterToViewProtocol 
extension NewsItemViewController: NewsItemPresenterToViewProtocol{
    func setArticleView(from article: Article) {
        contentView.setArticleView(from: article)
    }
}

// MARK: Extension - NewsItemRouterToViewProtocol
extension NewsItemViewController: NewsItemRouterToViewProtocol{
    func presentView(view: UIViewController) {
        present(view, animated: true, completion: nil)
    }

    func pushView(view: UIViewController) {
        navigationController?.pushViewController(view, animated: true)
    }
}

// MARK: Extension - NewsItemViewDelegate
extension NewsItemViewController: NewsItemViewDelegate{
    func linkButtonTapped() {
        presenter.linkButtonTapped()
    }
}
