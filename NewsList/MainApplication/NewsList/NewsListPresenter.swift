//
//  NewsListPresenter.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import Foundation

// MARK: Protocol - NewsListViewToPresenterProtocol (View -> Presenter)
protocol NewsListViewToPresenterProtocol: AnyObject {
	func viewDidLoad()
    func articleTapped(with indexPath: IndexPath)
    func fetchNextPage()
    func refreshData()
}

// MARK: Protocol - NewsListInteractorToPresenterProtocol (Interactor -> Presenter)
protocol NewsListInteractorToPresenterProtocol: AnyObject {
    func newIsFetched()
}

final class NewsListPresenter {

    // MARK: Properties
    var router: NewsListPresenterToRouterProtocol!
    var interactor: NewsListPresenterToInteractorProtocol!
    weak var view: NewsListPresenterToViewProtocol!
}

// MARK: Extension - NewsListViewToPresenterProtocol
extension NewsListPresenter: NewsListViewToPresenterProtocol {
    func refreshData() {
        interactor.clearArticlesData()
        viewDidLoad()
    }
    
    func fetchNextPage() {
        guard interactor.getArticles.count < interactor.getTotalCountArticles else { return }
        
        fetchNews()
    }
    
    func articleTapped(with indexPath: IndexPath) {
        interactor.addCounter(to: indexPath)
        router.navigateToArticle(withModel: interactor.getArticles[indexPath.row])
    }
    
    func viewDidLoad() {
        interactor.fetchArticleListFromCoreData(completion: {[weak self] result in
            
            if result { self?.newIsFetched() }
            
            self?.fetchNews()
        })
    }
}

// MARK: Extension - NewsListInteractorToPresenterProtocol
extension NewsListPresenter: NewsListInteractorToPresenterProtocol {
    func newIsFetched() {
        DispatchQueue.main.async {
            self.view.setArticlesData(self.interactor.getArticles)
        }
    }
}

private extension NewsListPresenter {
    private func fetchNews() {
        DispatchQueue.global(qos: .utility).async { [weak self] in
            self?.interactor.fetchNews()
        }
    }
}
