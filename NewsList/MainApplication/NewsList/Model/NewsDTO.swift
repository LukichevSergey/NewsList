//
//  NewsDTO.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//

import Foundation

struct NewsDTO: Codable {
    let status: String
    let totalResults: Int
    let articles: [ArticleDTO]
}

extension NewsDTO {
    struct ArticleDTO: Codable {
        let author: String?
        let title: String?
        let description: String?
        let url: String?
        let urlToImage: String?
        let publishedAt: String?
        let content: String?
    }
}
