//
//  NewsListInteractor.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import Foundation

// MARK: Protocol - NewsListPresenterToInteractorProtocol (Presenter -> Interactor)
protocol NewsListPresenterToInteractorProtocol: AnyObject {
    var getArticles: [Article] { get }
    var getTotalCountArticles: Int { get }
    
    func fetchArticleListFromCoreData(completion: @escaping (Bool) -> ())
    func fetchNews()
    func addCounter(to indexPath: IndexPath)
    func clearArticlesData()
}

final class NewsListInteractor {

    // MARK: Properties
    weak var presenter: NewsListInteractorToPresenterProtocol!
    private var coreData = CoreDataManager()
    private var nowFetching: Bool = false
    private var totalCountArticles: Int = 0
    private var fetchedPagesCount: Int = 0
    private var articles: [Article] = []
}

// MARK: Extension - NewsListPresenterToInteractorProtocol
extension NewsListInteractor: NewsListPresenterToInteractorProtocol {
    var getTotalCountArticles: Int {
        return totalCountArticles
    }
    
    var getArticles: [Article] {
        return articles
    }
    
    func fetchNews() {
        guard !nowFetching else { return }
        
        nowFetching = true
        
        NetworkNewsManager.shared.fetchData(page: fetchedPagesCount + 1) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let model):
                model.articles.forEach({ self.coreData.saveArticle(model: $0) })
                
                if self.fetchedPagesCount == 0 { self.totalCountArticles = model.totalResults }
                model.updateNewsArticles(&self.articles)
                
                self.fetchedPagesCount += 1
                self.presenter.newIsFetched()
            case .failure(let error):
                debugPrint(error)
            }
            
            self.nowFetching = false
        }
    }
    
    func addCounter(to indexPath: IndexPath) {
        articles[indexPath.row].addToCounter()
        coreData.addCounter(for: getArticles[indexPath.row])
    }
    
    func fetchArticleListFromCoreData(completion: @escaping (Bool) -> ()) {
        let articleList = coreData.fetchArticles()
        
        if !articleList.isEmpty {
            articles = articleList
            return completion(true)
        }
        
        completion(false)
    }
    
    func clearArticlesData() {
        articles.removeAll()
        totalCountArticles = 0
        fetchedPagesCount = 0
    }
}
