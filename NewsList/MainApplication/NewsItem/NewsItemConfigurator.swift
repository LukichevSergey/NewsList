//
//  NewsItemConfigurator.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import UIKit

final class NewsItemConfigurator {
    func configure(withModel model: Article) -> UIViewController {
        let view = NewsItemViewController()
        let presenter = NewsItemPresenter()
        let router = NewsItemRouter()
        let interactor = NewsItemInteractor(withModel: model)
        
        view.presenter = presenter

        presenter.router = router
        presenter.interactor = interactor
        presenter.view = view

        interactor.presenter = presenter
        
        router.view = view
        
        return view
    }
}
