//
//  NewsListViewController.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import UIKit

// MARK: Protocol - NewsListPresenterToViewProtocol (Presenter -> View)
protocol NewsListPresenterToViewProtocol: AnyObject {
    func setArticlesData(_ data: [Article])
}

// MARK: Protocol - NewsListRouterToViewProtocol (Router -> View)
protocol NewsListRouterToViewProtocol: AnyObject {
    func pushView(view: UIViewController)
}

final class NewsListViewController: UIViewController {
    
    // MARK: - Property
    var presenter: NewsListViewToPresenterProtocol!
    private lazy var contentView = NewsListView()

    // MARK: - init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func loadView() {
        view = contentView
        contentView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "News"
        presenter.viewDidLoad()
    }
}

// MARK: Extension - NewsListPresenterToViewProtocol 
extension NewsListViewController: NewsListPresenterToViewProtocol{
    func setArticlesData(_ data: [Article]) {
        contentView.setArticlesData(data)
    }
}

// MARK: Extension - NewsListRouterToViewProtocol
extension NewsListViewController: NewsListRouterToViewProtocol{
    func pushView(view: UIViewController) {
        navigationController?.pushViewController(view, animated: true)
    }
}

// MARK: Extension - NewsListViewDelegate
extension NewsListViewController: NewsListViewDelegate {
    func refreshData() {
        presenter.refreshData()
    }
    
    func tableOffsetInBottomPosition() {
        presenter.fetchNextPage()
    }
    
    func articleTapped(with indexPath: IndexPath) {
        presenter.articleTapped(with: indexPath)
    }
}
