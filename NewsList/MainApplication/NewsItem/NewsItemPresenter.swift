//
//  NewsItemPresenter.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import Foundation

// MARK: Protocol - NewsItemViewToPresenterProtocol (View -> Presenter)
protocol NewsItemViewToPresenterProtocol: AnyObject {
	func viewDidLoad()
    func linkButtonTapped()
}

// MARK: Protocol - NewsItemInteractorToPresenterProtocol (Interactor -> Presenter)
protocol NewsItemInteractorToPresenterProtocol: AnyObject {

}

final class NewsItemPresenter {

    // MARK: Properties
    var router: NewsItemPresenterToRouterProtocol!
    var interactor: NewsItemPresenterToInteractorProtocol!
    weak var view: NewsItemPresenterToViewProtocol!
}

// MARK: Extension - NewsItemViewToPresenterProtocol
extension NewsItemPresenter: NewsItemViewToPresenterProtocol {
    func linkButtonTapped() {
        router.navigateToWebView(withLink: interactor.articleModel.url ?? "")
    }
    
    func viewDidLoad() {
        DispatchQueue.main.async {
            self.view.setArticleView(from: self.interactor.articleModel)
        }
    }
}

// MARK: Extension - NewsItemInteractorToPresenterProtocol
extension NewsItemPresenter: NewsItemInteractorToPresenterProtocol {
    
}
