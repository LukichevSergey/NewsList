//
//  ViewedArticleToCoreDataManagerProtocol.swift
//  NewsList
//
//  Created by Сергей Лукичев on 04.02.2023.
//

import Foundation
import CoreData

extension CoreDataManager: ArticlesToCoreDataManagerProtocol {

    func fetchArticles() -> [Article] {
        let fetchRequest: NSFetchRequest<ViewedArticles> = ViewedArticles.fetchRequest()
        
        do {
            let result = try context.fetch(fetchRequest)
            if result.count > 100 { clearTableViewedArticle() }
            if !result.isEmpty {
                return result.map {( .init(author: $0.author,
                                           title: $0.title,
                                           articleDescription: $0.articleDescription,
                                           url: $0.url,
                                           urlToImage: $0.urlToImage,
                                           publishedAt: $0.publishedAt,
                                           content: $0.content,
                                           counter: Int($0.counter) ))}
            }
        } catch {
            debugPrint(error.localizedDescription)
        }

        return []
    }

    func saveArticle(model: Article) {
        let fetchRequest: NSFetchRequest<ViewedArticles> = ViewedArticles.fetchRequest()
        guard let entityViewedArticle = NSEntityDescription.entity(forEntityName: "ViewedArticles", in: context) else { return }
        
        do {
            let result = try context.fetch(fetchRequest)
            if result.first(where: {$0.title == model.title && $0.publishedAt == model.publishedAt}) == nil {
                let newItem = ViewedArticles(entity: entityViewedArticle, insertInto: context)
                newItem.author = model.author
                newItem.title = model.title
                newItem.articleDescription = model.articleDescription
                newItem.url = model.url
                newItem.urlToImage = model.urlToImage
                newItem.publishedAt = model.publishedAt
                newItem.content = model.content
                newItem.counter = Int16(model.counter)
                
                try context.save()
            }
        } catch {
            debugPrint(error.localizedDescription)
        }
    }
    
    func addCounter(for model: Article) {
        let fetchRequest: NSFetchRequest<ViewedArticles> = ViewedArticles.fetchRequest()
        
        do {
            let result = try context.fetch(fetchRequest)
            result.first(where: {$0.title == model.title && $0.publishedAt == model.publishedAt})?.counter += 1
        } catch {
            debugPrint(error.localizedDescription)
        }
        
        do {
            try context.save()
        } catch {
            debugPrint(error.localizedDescription)
        }
    }

    private func clearTableViewedArticle() {

        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ViewedArticles")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)

        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            debugPrint(error.localizedDescription)
        }
    }
}
