//
//  NewsItemView.swift
//  NewsList
//
//  Created by Сергей Лукичев on 04.02.2023.
//

import UIKit

private enum Constants {
    static let titleFontSize: CGFloat = 18
    static let contentFontSize: CGFloat = 14
    static let horizontalConstraintInset: CGFloat = 20
    static let imageHeight: CGFloat = 150
}

protocol NewsItemViewDelegate: AnyObject {
    func linkButtonTapped()
}

protocol NewsItemViewInterface: AnyObject {
    func setArticleView(from article: Article)
}

final class NewsItemView: UIView {
    
    weak var delegate: NewsItemViewDelegate?
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: Constants.titleFontSize, weight: .bold)
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    private lazy var mainImage: UIImageView = {
        let mainImage = UIImageView()
        mainImage.contentMode = .scaleAspectFit
        mainImage.translatesAutoresizingMaskIntoConstraints = false
        
        return mainImage
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: Constants.titleFontSize, weight: .regular)
        
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .left
        label.numberOfLines = 1
        label.font = .systemFont(ofSize: Constants.titleFontSize, weight: .regular)
        
        return label
    }()
    
    private lazy var sourceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.blue, for: .normal)
        button.setTitle("Ссылка", for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(linkButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var commonStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titleLabel, sourceLabel, dateLabel, descriptionLabel, linkButton])
        stack.axis = .vertical
        stack.alignment = .leading
        stack.distribution = .fill
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    @objc private func linkButtonTapped() {
        delegate?.linkButtonTapped()
    }
}

extension NewsItemView {
    private func commonInit() {
        backgroundColor = .white
        addSubview(mainImage)
        NSLayoutConstraint.activate([
            mainImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalConstraintInset),
            mainImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalConstraintInset),
            mainImage.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            mainImage.heightAnchor.constraint(equalToConstant: Constants.imageHeight)
        ])
        
        addSubview(commonStack)
        NSLayoutConstraint.activate([
            commonStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalConstraintInset),
            commonStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalConstraintInset),
            commonStack.topAnchor.constraint(equalTo: mainImage.bottomAnchor, constant: Constants.horizontalConstraintInset),
        ])
    }
}

extension NewsItemView: NewsItemViewInterface {
    func setArticleView(from article: Article) {
        titleLabel.text = article.title
        descriptionLabel.text = article.content
        dateLabel.text = article.publishedAt?.dateWithFullMonthWordFormatted
        sourceLabel.text = article.author
        if let imageUrl = article.urlToImage {
            mainImage.loadImage(from: imageUrl)
        }
    }
}
