//
//  CoreDataManager.swift
//  NewsList
//
//  Created by Сергей Лукичев on 04.02.2023.
//

import CoreData

protocol ArticlesToCoreDataManagerProtocol: AnyObject {
    func fetchArticles() -> [Article]
    func saveArticle(model: Article)
    func addCounter(for model: Article)
}

final class CoreDataManager {
    var context: NSManagedObjectContext
    
    var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "NewsList")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            } else {
                storeDescription.shouldMigrateStoreAutomatically = true
            }
        })
        return container
    }()
    
    init() {
        self.context = persistentContainer.viewContext
    }
}
