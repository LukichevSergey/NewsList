//
//  NewsDTO + Mapping.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//

import Foundation

extension NewsDTO {
    func toDomain() -> News {
        return .init(status: status,
                     totalResults: totalResults,
                     articles: articles.map({ $0.toDomain()
        }))
    }
}

extension NewsDTO.ArticleDTO {
    func toDomain() -> Article {
        return .init(author: author,
                     title: title,
                     articleDescription: description,
                     url: url,
                     urlToImage: urlToImage,
                     publishedAt: publishedAt,
                     content: content)
    }
}
