//
//  WKWebViewController.swift
//  NewsList
//
//  Created by Сергей Лукичев on 04.02.2023.
//

import UIKit
import WebKit

final class WKWebViewController: UIViewController {
    
    private var url: String = ""
    
    private var webView: WKWebView = {
        let webView = WKWebView()
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        return webView
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    init(with url: String) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    func configureUI() {
        view.addSubview(webView)
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        configureUI()
        loadWebView(withWebArrress: url)
    }
    
    func loadWebView(withWebArrress webAddress: String) {
        if let url = URL(string: webAddress) {
            webView.load(URLRequest(url: url))
        }
    }
}
