//
//  NewsListManager.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//

import UIKit

final class NetworkNewsManager {
    static let shared = NetworkNewsManager()
    private init() {}
    
    func fetchData(page: Int, completion: @escaping (Result<News, Error>) -> Void) {
        let url = URL(string: "https://newsapi.org/v2/everything?q=apple&from=2023-08-10&to=2023-08-15&pageSize=20&page=\(page)&sortBy=popularity&apiKey=530653e4a02144378e6829279aa7c8c9")!
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                completion(.failure(NSError(domain: "", code: 0, userInfo: nil)))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let news = try decoder.decode(NewsDTO.self, from: data)
                completion(.success(news.toDomain()))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
}
