//
//  NewsListRouter.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import Foundation

// MARK: Protocol - NewsListPresenterToRouterProtocol (Presenter -> Router)
protocol NewsListPresenterToRouterProtocol: AnyObject {
    func navigateToArticle(withModel model: Article)
}

final class NewsListRouter {

    // MARK: Properties
    weak var view: NewsListRouterToViewProtocol!
}

// MARK: Extension - NewsListPresenterToRouterProtocol
extension NewsListRouter: NewsListPresenterToRouterProtocol {
    func navigateToArticle(withModel model: Article) {
        let articleViewController = NewsItemConfigurator().configure(withModel: model)
        view.pushView(view: articleViewController)
    }
}
