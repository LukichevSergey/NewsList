//
//  ViewedArticles+CoreDataProperties.swift
//  NewsList
//
//  Created by Сергей Лукичев on 04.02.2023.
//

import Foundation
import CoreData

extension ViewedArticles {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ViewedArticles> {
        return NSFetchRequest<ViewedArticles>(entityName: "ViewedArticles")
    }
    
    @NSManaged public var articleDescription: String?
    @NSManaged public var author: String?
    @NSManaged public var content: String?
    @NSManaged public var counter: Int16
    @NSManaged public var publishedAt: String?
    @NSManaged public var title: String?
    @NSManaged public var url: String?
    @NSManaged public var urlToImage: String?
}


extension ViewedArticles : Identifiable {
    
}
