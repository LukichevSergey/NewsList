//
//  NewsListConfigurator.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import UIKit

final class NewsListConfigurator {
    func configure() -> UIViewController {
        let view = NewsListViewController()
        let presenter = NewsListPresenter()
        let router = NewsListRouter()
        let interactor = NewsListInteractor()
        
        view.presenter = presenter

        presenter.router = router
        presenter.interactor = interactor
        presenter.view = view

        interactor.presenter = presenter
        
        router.view = view
        
        return view
    }
}
