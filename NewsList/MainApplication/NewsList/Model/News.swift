//
//  News.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//

import Foundation
import Combine

typealias Article = News.Article

struct News {

    let status: String
    let totalResults: Int
    let articles: [Article]
    
    init(status: String, totalResults: Int, articles: [News.Article]) {
        self.status = status
        self.totalResults = totalResults
        self.articles = articles
    }
}

extension News {

    final class Article: Hashable  {

        static func == (lhs: News.Article, rhs: News.Article) -> Bool {
            return lhs.hashValue == rhs.hashValue
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(uuid)
            hasher.combine(title)
            hasher.combine(publishedAt)
            hasher.combine(counter)
        }
        
        let uuid = UUID()
        let author: String?
        let title: String?
        let articleDescription: String?
        let url: String?
        let urlToImage: String?
        let publishedAt: String?
        let content: String?
        
        @Published var counter: Int = 0
        
        init(author: String? = nil,
             title: String? = nil,
             articleDescription: String? = nil,
             url: String? = nil,
             urlToImage: String? = nil,
             publishedAt: String? = nil,
             content: String? = nil,
             counter: Int = 0) {
            self.author = author
            self.title = title
            self.articleDescription = articleDescription
            self.url = url
            self.urlToImage = urlToImage
            self.publishedAt = publishedAt
            self.content = content
            self.counter = counter
        }
    }
}

extension News {
    func updateNewsArticles(_ currentArticles: inout [Article]) {
        for newArticle in articles {
            if let index = currentArticles.firstIndex(where: { $0.title == newArticle.title && $0.publishedAt == newArticle.publishedAt }) {
                let counter = currentArticles[index].counter
                currentArticles[index] = newArticle
                currentArticles[index].counter = counter
            } else {
                currentArticles.append(newArticle)
            }
        }
    }
}

extension News.Article {
    func addToCounter() {
        counter += 1
    }
}
