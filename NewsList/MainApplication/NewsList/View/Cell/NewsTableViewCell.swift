//
//  NewsTableViewCell.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//

import UIKit
import Combine

private enum Constants {
    static let titleFontSize: CGFloat = 18
    static let titleHeight: CGFloat = 43
    static let imageHeight: CGFloat = 100
    static let counterInsets: CGFloat = 15
    static let contentVerticalInset: CGFloat = 10
    static let contentHorizontalInset: CGFloat = 20
    static let contentViewInsets: CGFloat = 10
    static let contentViewCornerRadius: CGFloat = 12
}

final class NewsTableViewCell: UITableViewCell {
    
    private var subscriptions = Set<AnyCancellable>()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 2
        label.font = .systemFont(ofSize: Constants.titleFontSize, weight: .bold)
        
        return label
    }()
    
    private lazy var newsImage: UIImageView = {
        let newsImage = UIImageView()
        newsImage.contentMode = .scaleAspectFit
        newsImage.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: Constants.imageHeight)
        newsImage.image = UIImage(named: "placeholder")
        
        return newsImage
    }()
    
    private lazy var counterLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = .red
        label.textAlignment = .center
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var verticalStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titleLabel, newsImage])
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    static let reuseIdentifier = String(describing: NewsTableViewCell.self)
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        contentView.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: Constants.contentViewInsets,
                                                                     left: Constants.contentViewInsets,
                                                                     bottom: Constants.contentViewInsets,
                                                                     right: Constants.contentViewInsets))
        contentView.layer.cornerRadius = Constants.contentViewCornerRadius
    }
}

extension NewsTableViewCell {
    func configure(with model: Article) {
        contentView.backgroundColor = .lightGray
        commonInit()
        
        titleLabel.text = model.title
        
        model.$counter.sink { [weak self] counter in
            self?.counterLabel.text = "\(counter)"
        }.store(in: &subscriptions)
        
        counterLabel.text = "\(model.counter)"
        if let imageUrl = model.urlToImage {
            newsImage.loadImage(from: imageUrl)
        }
    }
    
    private func commonInit() {
        contentView.addSubview(verticalStack)
        NSLayoutConstraint.activate([
            verticalStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.contentHorizontalInset),
            contentView.trailingAnchor.constraint(equalTo: verticalStack.trailingAnchor, constant: Constants.contentHorizontalInset),
            verticalStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.contentVerticalInset),
            contentView.bottomAnchor.constraint(equalTo: verticalStack.bottomAnchor, constant: Constants.contentVerticalInset),
        ])
        
        contentView.addSubview(counterLabel)
        NSLayoutConstraint.activate([
            counterLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.counterInsets),
            counterLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constants.counterInsets)
        ])
        
        titleLabel.heightAnchor.constraint(equalToConstant: Constants.titleHeight).isActive = true
    }
}
