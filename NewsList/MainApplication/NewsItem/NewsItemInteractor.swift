//
//  NewsItemInteractor.swift
//  NewsList
//
//  Created by Сергей Лукичев on 03.02.2023.
//  
//

import Foundation

// MARK: Protocol - NewsItemPresenterToInteractorProtocol (Presenter -> Interactor)
protocol NewsItemPresenterToInteractorProtocol: AnyObject {
    var articleModel: Article { get }
}

final class NewsItemInteractor {

    // MARK: Properties
    weak var presenter: NewsItemInteractorToPresenterProtocol!
    
    let model: Article

    init(withModel model: Article) {
        self.model = model
    }
}

// MARK: Extension - NewsItemPresenterToInteractorProtocol
extension NewsItemInteractor: NewsItemPresenterToInteractorProtocol {
    var articleModel: Article {
        return model
    }
}
