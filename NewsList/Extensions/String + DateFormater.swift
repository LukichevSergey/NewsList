//
//  String + DateFormater.swift
//  NewsList
//
//  Created by Сергей Лукичев on 04.02.2023.
//

import Foundation

extension String {
    var dateWithFullMonthWordFormatted: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = dateFormatter.date(from: self) else { return self }
        dateFormatter.dateFormat = " dd MMMM yyyy"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
}
